CWD=/go/src/gitlab.com/YakovLachin/grpcserver
IMAGE=gitlab.com/yakovlachin/grpcserver
CODE_GENERATOR=grpc-go
test:
	@docker run --rm -v $(CURDIR):/app -w /app golang:1.8-alpine go test ./...

format:
	@docker run --rm -v $(CURDIR):/app -w /app golang:1.8-alpine gofmt -w /app

image: service
	@docker build -t $(IMAGE) .

up:
	@docker-compose up

service:
	@docker run --rm -it -v $(CURDIR):$(CURDIR) -w $(CURDIR) \
	grpc-go /bin/bash -c 'mkdir server && protoc --proto_path=./proto/ --go_out=plugins=grpc:./server service.proto'


create-network:
	@-docker network create app-net

.DEFAULT_GOAL := image