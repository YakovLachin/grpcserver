package server

import (
	"golang.org/x/net/context"
	"gitlab.com/YakovLachin/grpcserver/service"
)

func (s *srv) SimpleReq(context.Context, *service.MessageReq) (*service.MessageRes, error) {
	panic("implement me")
}
