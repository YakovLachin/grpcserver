package server

import (
	"gitlab.com/YakovLachin/grpcserver/service"
)

type Srv interface {
	service.SimpleServiceServer
}

func NewServer() Srv {
	return &srv{}
}

type srv struct{}

