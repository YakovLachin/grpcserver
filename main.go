package main

import (
	"fmt"
	"net"
	"os"
	"time"
	"net/http"

	"google.golang.org/grpc"

	"gitlab.com/YakovLachin/grpcserver/server"
	"gitlab.com/YakovLachin/grpcserver/service"
)

func main() {
	fmt.Println("Starting Service ...")
	mux := http.NewServeMux()
	var opts []grpc.ServerOption
	grpcServ := grpc.NewServer(opts...)
	service.RegisterSimpleServiceServer(grpcServ, server.NewServer())
	ntSrv, err := net.Listen("tcp", "0.0.0.0:9000")
	if err != nil {
		fmt.Println("Fail tcp Listen port")
		os.Exit(1)
	}
	if err != nil {
		fmt.Println("Fail tcp Listen port")
		os.Exit(1)
	}
	go grpcServ.Serve(ntSrv)

	s := &http.Server{
		Addr:         "0.0.0.0:8080",
		Handler:      mux,
		ReadTimeout:  time.Duration(5 * time.Second),
		WriteTimeout: time.Duration(5 * time.Second),
	}
	mux.HandleFunc("/", Helloworld)

	sigchan := make(chan os.Signal)
	go s.ListenAndServe()
	<-sigchan
}

func Helloworld(writer http.ResponseWriter, req *http.Request) {
	writer.Write([]byte("hello from srv\n"))
}
