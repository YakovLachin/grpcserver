FROM golang:1.8.3-alpine as builder
WORKDIR /go/src/gitlab.com/YakovLachin/grpcserver
COPY . .
RUN GOOS=linux GOARCH=amd64 go build -v -o rel/grpcserver

FROM alpine:3.6
WORKDIR /grpcserver
EXPOSE 8080
EXPOSE 9000
COPY --from=builder /go/src/gitlab.com/YakovLachin/grpcserver/rel/grpcserver ./
CMD ["./grpcserver"]
